(function(){
	var app = angular.module('application',['ui.router', 'ui.bootstrap', 'angular-loading-bar', 'controllers', 'directives']);


	app.config(function($stateProvider, $urlRouterProvider) {
	  $stateProvider
	    .state('home', {
	        url: '/home',
	        templateUrl: 'templates/home.html'
	    })
	    .state('list', {
	        url: '/list',
	        templateUrl: 'templates/list.html',
	        controller: 'ListCtrl'
	    })
	    .state('list.item', {
	        url: '/:item',
	        templateUrl: 'templates/list.item.html',
	        controller: function($scope, $stateParams) {
	            $scope.item = $stateParams.item;
	        }
	    })
	  $urlRouterProvider.otherwise('/home');
	})

})();